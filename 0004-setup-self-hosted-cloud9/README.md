---
Title: Setting up a powerful self-hosted IDE in the cloud
---

# Setting up a powerful self-hosted IDE in the cloud


These files belong to the blog article available at:

  * http://gbraad.nl/blog/setting-up-a-powerful-self-hosted-ide-in-the-cloud.html
  * https://gitlab.com/gbraad/blog-content/blob/master/0004-setup-self-hosted-cloud9.md
