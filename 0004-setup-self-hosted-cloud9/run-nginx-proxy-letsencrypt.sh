#!/bin/sh
docker run -d \
   --name nginx-proxy-letsencrypt \
   -v /etc/nginx/certs:/etc/nginx/certs:rw \
   --volumes-from nginx-proxy \
   -v /var/run/docker.sock:/var/run/docker.sock:ro \
   jrcs/letsencrypt-nginx-proxy-companion