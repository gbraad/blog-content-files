#!/bin/sh -x
set -e

NAME=${1:-"test"}
HOST=${2:-${NAME}".local"}
USER=${3:-"nobody@local"}
PASS=${4:-"secrete"}
FLAVOR=${5:-"u1604"}  # c7, f24, u1604
VOLUME=${6:-"/var/workspaces/c9ide-"${NAME}}

mkdir -p ${VOLUME}
docker run -d \
   --name ${NAME} \
   -v ${VOLUME}:/workspace:rw \
   -e "VIRTUAL_HOST=${HOST}" \
   -e "LETSENCRYPT_HOST=${HOST}" \
   -e "LETSENCRYPT_EMAIL=${USER}" \
   -e "USERNAME=${USER}" \
   -e "PASSWORD=${PASS}" \
   gbraad/c9ide:${FLAVOR}